//
//  ViewController.swift
//  dz2part2
//
//  Created by Максим Бойко on 3/16/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        main(startprice: 24, percent: 1.06, year: 1826)
        
    }
    func main(startprice: Double,percent: Double,year: Int) {
        var sum = startprice
        for i in 1...(2019-year){
            sum = sum * percent
        }
        print("решение 1 задачи ",sum)
    }

}

